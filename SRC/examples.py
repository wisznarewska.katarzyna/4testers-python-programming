def get_4tester_email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


print(get_4tester_email("Janusz", "Nowak"))
print(get_4tester_email("Barbara", "Kowalska"))



def gamer_description(description):
    print(f"The player {description['nick']} is of type {description['type']} and has {description['exp_points']} EXP")

description = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000
}

gamer_description(description)