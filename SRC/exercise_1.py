def square(numbers):
    return numbers ** 2


result_1 = square(16)
result_2 = square(0)
result_3 = square(2.55)
print(f"Square (16) is: {result_1},Square (0) is: {result_2},Square (2.55) is: {result_3}")


def volume_of_cuboid(a, b, c):
    return a * b * c


result = volume_of_cuboid(3, 5, 7)
print(f"Volume of cuboid is: {result}")


def calculate_celsius_to_fahrenheit(temp):
    return temp * 9 / 5 + 32


result_f = calculate_celsius_to_fahrenheit(20)
print(f" 20 Celsius is {result_f} Fahrenheit")
