my_name = "Kasia"
my_age = 34
my_email = "katarzyna.lukasik@o2.pl"

print(my_name)
print(my_age)
print(my_email)

# Below is the description of my friend
friend_name = "Stefan"
friend_age = 70
number_of_pets = 10
has_driving_license = True
years_of_friendship = 3.5

print(friend_name)
print(friend_age)
print(number_of_pets)
print(has_driving_license)
print(years_of_friendship)
print (friend_name, friend_age, number_of_pets, has_driving_license, years_of_friendship, sep=", ")


