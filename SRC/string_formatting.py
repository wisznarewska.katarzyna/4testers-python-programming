my_name = "Kasia"
favourite_movie = "Titanic"
favourite_actor = "Brad Pitt"

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)


def print_welcome(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")


print_welcome("Michał", "Toruń")
print_welcome("beata", "gdynia")


def generate_email(firstname, lastname):
    return f"{firstname.lower()}.{lastname.lower()}@4te sters.pl"


print(generate_email("Janusz", "Nowak"))
print(generate_email("Barbara", "Kowalska"))
