my_name = "Kasia"
my_age = 34
my_email = "katarzyna.lukasik@o2.pl"

print(my_name)
print(my_age)
print(my_email)

first_name = "Kasia"
last_name = "Wisznarewska"
my_email = "katarzyna.lukasik@o2.pl"

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " +last_name + ". Mój email to " +my_email + "."
print(my_bio)

#F-string

my_bio_using_f_string = f"Mam na imię {first_name}. \nMoje nazwisko to {last_name}. Mój email to {my_email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}")

###Algebra###
area_of_a_circe_with_radius_5 = 3.14 * 5 ** 2
print("area_of_a_circe_with_radius_5:", area_of_a_circe_with_radius_5)


# Below is the description of my friend
friend_name = "Stefan"
friend_age = 70
number_of_pets = 10
has_driving_license = True
years_of_friendship = 3.5

print(friend_name)
print(friend_age)
print(number_of_pets)
print(has_driving_license)
print(years_of_friendship)
print (friend_name, friend_age, number_of_pets, has_driving_license, years_of_friendship, sep=", ")

