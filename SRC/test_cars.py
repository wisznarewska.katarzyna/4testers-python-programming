from cars import get_country_of_a_car_brand, get_gas_usage_for_distance


def test_get_country_for_japanese_car():
    assert get_country_of_a_car_brand('Toyota') == 'Japan'


def test_get_country_for_japanese_car_capitalized():
    assert get_country_of_a_car_brand('toyota') == 'Japan'


def test_get_country_for_german_car():
    assert get_country_of_a_car_brand('BMW') == 'Germany'


def test_get_country_for_german_car_capitalized():
    assert get_country_of_a_car_brand('bmw') == 'Germany'


def test_get_country_for_french_car():
    assert get_country_of_a_car_brand('Renault') == 'France'


def test_get_country_for_french_car_capitalized():
    assert get_country_of_a_car_brand('peugeot') == 'France'


# test_for_a_function_calculating_gas_usage

def test_gas_usage_for_100_kilometers_distance_driven():
    assert get_gas_usage_for_distance(100, 8.5) == 8.5


def test_gas_usage_for_zero_kilometers_distance_driven():
    assert get_gas_usage_for_distance(0, 8.5) == 0

def test_gas_usage_for_negative_distance_driven():
    assert get_gas_usage_for_distance(-50, 8.5) == 0
